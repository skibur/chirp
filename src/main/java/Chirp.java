import org.apache.commons.cli.*;
import org.apache.commons.lang3.StringUtils;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;

import java.util.Scanner;

public class Chirp {
    public static void main(String args[]) {
        String progName = "Chirp";
        String version = "0.0.1";

        Options options = new Options();
        HelpFormatter formatter = new HelpFormatter();

        options.addOption("h", "Show help options");
        options.addOption("v", "Display version number");
        options.addOption("y", "Answer yes to all questions");
        options.addOption("m", true, "Message to send. (280 characters)");

        CommandLineParser parser = new DefaultParser();

        try {
            CommandLine cmd = parser.parse(options, args);
            if (cmd.hasOption("h")) {
                formatter.printHelp(progName, options, true);
                System.exit(0);
            } else if (cmd.hasOption("v")) {
                System.out.println(progName + " v" + version);
                System.exit(0);
            } else if (cmd.hasOption("m")) {
                TwitterFactory factory = new TwitterFactory();
                Twitter twitter = factory.getInstance();
                String message = cmd.getOptionValue("m");
                if (!StringUtils.isBlank(message) && message.length() <= 280) {
                    try {
                        twitter.verifyCredentials();
                        String answer;
                        System.out.println("Tweet the following:\n");
                        System.out.println(message + "\n");
                        if (!cmd.hasOption("y")) {
                            Scanner reader = new Scanner(System.in);
                            System.out.print("[y/n]: ");
                            answer = reader.next();
                            reader.close();
                        } else {
                            answer = "y";
                        }

                        if (answer.contentEquals("y") || cmd.hasOption("y")) {
                            twitter.updateStatus(message);
                            System.out.println("Tweet sent successfully");
                            System.exit(0);
                        } else if (answer.contentEquals("n")) {
                            System.exit(0);
                        }
                    } catch (IllegalStateException e) {
                        System.out.println("Missing twitter4j.properties file.");
                        System.exit(1);
                    } catch (TwitterException e) {
                        System.out.println("Server refused: duplicate tweet or other.");
                        System.exit(1);
                    }
                } else {
                    System.out.println("Unsuccessful tweet sent. Tweet length: " + message.length());
                    System.exit(1);
                }
            } else {
                System.out.println("Try \'" + progName + " -h\' for more information");
                System.exit(0);
            }
        } catch (ParseException e) {
            System.out.println(progName + ": " + e.getMessage());
            System.out.println("Try \'" + progName + " -h\' for more information");
            System.exit(1);
        }
    }
}